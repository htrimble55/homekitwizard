# README #

Home Wizard is an iOS 8 Swift application designed to get you started with Apple's HomeKit.  There is a companion article for the application at http://dev.iachieved.it/iachievedit/?p=478 and http://dev.iachieved.it/iachievedit/?p=651.

As of September 14, 2014 you will need Xcode 6 GM Seed on your Mac and iOS 8 GM Seed on your iPhone to make full use of this application.  Please note that Siri integration with HomeKit is broken as of this writing.

### License ###

Home Wizard is licensed with an Apache License, Version 2.0.  Details of the license can be found at http://www.apache.org/licenses/LICENSE-2.0.  

### Who do I talk to? ###

For any questions or comments, please contact joe@iachieved.it.