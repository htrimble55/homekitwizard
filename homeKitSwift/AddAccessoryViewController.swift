//
//  AddAccessoryViewController.swift
//  homeKitSwift
//
//  Copyright (c) 2014 iAchieved.it LLC.  All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

import Foundation
import UIKit
import HomeKit

class AddAccessoryViewController : UIViewController, HMAccessoryBrowserDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
  
  var homes: [AnyObject]!
  var accessoryBrowser:  HMAccessoryBrowser?
  var accessories:       [HMAccessory]!
  
  @IBOutlet weak var accessoryPickerView: UIPickerView!
  @IBOutlet weak var locationPickerView: UIPickerView!

  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(animated: Bool)  {
    super.viewDidAppear(animated)
    
    // Get homes
    let hm = HomeManagerControllerSharedInstanced
    self.homes = hm.myHomes
    
    // Set up picker views
    self.locationPickerView.dataSource = self
    self.locationPickerView.delegate   = self
    
    self.accessoryPickerView.dataSource = self
    self.accessoryPickerView.delegate   = self

    // Turn on looking for accessories
    self.accessories = []
    self.accessoryBrowser = HMAccessoryBrowser()
    self.accessoryBrowser!.delegate = self
    self.accessoryBrowser!.startSearchingForNewAccessories()
    
    let nc = NSNotificationCenter.defaultCenter()
    nc.addObserver(self, selector: "addAccessoryError:", name: kAddAccessoryError, object:nil)
    nc.addObserver(self, selector: "addAccessoryOK:", name: kAddAccessoryOK, object:nil)
    nc.addObserver(self, selector: "homesUpdated", name: kHomesUpdated, object:nil)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
  
  override func viewDidDisappear(animated: Bool) {
    // Turn off looking for accessories
    self.accessoryBrowser!.stopSearchingForNewAccessories()
    
    let nc = NSNotificationCenter.defaultCenter()
    nc.removeObserver(self)

  }
  
  func accessoryBrowser(
    browser: HMAccessoryBrowser!,
    didFindNewAccessory accessory: HMAccessory!) {
      SLogVerbose("Found accessory:  \(accessory)")
      
      // If we don't know about this accessory (we go by identifier, not the name)
      // then ignore it
      let alreadyFound = self.accessories.filter({a in a.identifier == accessory.identifier})
      if alreadyFound.count > 0 {
        SLogVerbose("Accessory already found")
      } else {
        self.accessories.append(accessory)
        self.accessoryPickerView.reloadAllComponents()
      }
  }
  
  func accessoryBrowser(browser: HMAccessoryBrowser!, didRemoveNewAccessory accessory: HMAccessory!) {
    ENTRY_LOG()
    EXIT_LOG()
  }
  
  @IBAction func addAccessory(sender: AnyObject) {
    
    NSLog("addAccessory")
    
    // Get accessory name
    var aRow = self.accessoryPickerView.selectedRowInComponent(0)
    var accessory:HMAccessory = self.accessories[aRow]
    
    // Get home and room name
    var homeRow     = self.locationPickerView.selectedRowInComponent(0)
    var home:HMHome = self.homes[homeRow] as HMHome
    
    var roomRow     = self.locationPickerView.selectedRowInComponent(1)
    var room:HMRoom = home.rooms[roomRow] as HMRoom
    
    let hm = HomeManagerControllerSharedInstanced
    hm.addAccessory(accessory, toHome: home.name, inRoom: room.name)
    
  }
  
  // MARK:  Notification Receivers
  func homesUpdated () {
    NSLog("homesUpdated")
    
    /* We've found that if our homes are updated while we need to refresh */
    self.homes = HomeManagerControllerSharedInstanced.myHomes
    self.locationPickerView.reloadAllComponents()
  }
  
  func addAccessoryOK(notification:NSNotification) {
    let userInfo:Dictionary<String,String> = notification.userInfo as Dictionary<String,String>
    let messageString = userInfo["message"]!
    let accessoryName = userInfo["accessory"]!
    
    var alert = UIAlertController(title: "HomeKit", message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)
    
    NSLog("Remove \(accessoryName) from picker list")
    
    let filtered = self.accessories.filter({a in a.name != accessoryName})
    self.accessories = filtered
    self.accessoryPickerView.reloadAllComponents()


  }
  
  func addAccessoryError(notification:NSNotification) {
    let userInfo:Dictionary<String,String> = notification.userInfo as Dictionary<String,String>
    let messageString = userInfo["message"]!
    let accessoryName = userInfo["accessory"]!
    
    var alert = UIAlertController(title: "HomeKit", message: messageString, preferredStyle: UIAlertControllerStyle.Alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
    self.presentViewController(alert, animated: true, completion: nil)

    
  }
  
  // MARK:  UIPickerViewDataSource
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    
    // LocationPickerView
    if (pickerView == self.locationPickerView) {
      // Column for home and room
      return 2
    }
    else {
      // AccessoryPickerView

      return 1
    }
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    if pickerView == self.locationPickerView {
      if (component == 0) {
        return self.homes.count
      } else {
        
        var row = pickerView.selectedRowInComponent(0)
        var home:HMHome = self.homes[row] as HMHome
        return home.rooms.count
        
        
      }
    } else {
      // AccessoryPickerView
      return self.accessories.count
    }
  }
  
  // MARK:  UIPickerViewDelegate
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
    if pickerView == self.locationPickerView {
    if (component == 1) {
      // Select room based upon what home is already selected
      var homeRow = pickerView.selectedRowInComponent(0)
      var home:HMHome = self.homes[homeRow] as HMHome
      var room:HMRoom = home.rooms[row] as HMRoom
      return room.name
    } else {
      var home:HMHome = self.homes[row] as HMHome
      return home.name
    }
    } else {
      // AccessoryPickerView
      var accessory:HMAccessory = self.accessories[row] as HMAccessory
      return accessory.name
    }
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    if pickerView == self.locationPickerView {
      // If component 0 (home) changed then reload the data
      pickerView.reloadAllComponents()
    }
  }


  
}
