//
//  HomeManagerController.swift
//  homeKitSwift
//
//  Copyright (c) 2014 iAchieved.it LLC.  All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//


import Foundation
import HomeKit
import UIKit

class HomeManagerController : NSObject, HMHomeManagerDelegate {
  
  class var sharedInstance:HomeManagerController {
    return HomeManagerControllerSharedInstanced
  }
  
  var homeManager:       HMHomeManager
  var myHomes:           [AnyObject]!
  var notificationCenter:NSNotificationCenter!
  
  override init() {
    
    NSLog("Running init for HomeManagerController")
    
    self.homeManager      = HMHomeManager()
    self.myHomes          = []
    
    super.init()
    
    self.homeManager.delegate      = self
    self.notificationCenter = NSNotificationCenter.defaultCenter()

  }
  
  func addHome(homeName:String!,primary:Bool = false) {
    
    self.homeManager.addHomeWithName(homeName, {
      (home:  HMHome?, error:  NSError?) -> Void in
      
      if let addHomeError = error {
        NSLog("Error:  \(addHomeError.localizedDescription)")
        self.notificationCenter.postNotificationName(kAddHomeError,
                                                     object: nil,
                                                     userInfo:["message":"Error adding home \(homeName):  \(addHomeError.localizedDescription)"])
      } else {
        NSLog("Added home:  \(home)")
        self.notificationCenter.postNotificationName(kAddHomeOK, object: nil, userInfo:["message":"Home \(homeName) has been added"])
        
        // If primary, then make primary
        if primary {
          SLogVerbose("Make primary home")
          self.homeManager.updatePrimaryHome(home, completionHandler: {(error:NSError?) -> Void in
          SLogVerbose("update")
          })
        }

      }
      
      })
  }
  
  func addRoom(roomName:String!, toHome homeName:String!) {
    
    // Find home by name
    var home:HMHome = self.myHomes.filter({h in h.name == homeName}).first as HMHome
    
    home.addRoomWithName(roomName, completionHandler: {
      (room:HMRoom!, error:NSError!) -> Void in
      if let e = error {
        self.notificationCenter.postNotificationName(kAddRoomError, object: nil, userInfo:["message":"Error adding room \(roomName)"])
      } else {
        self.notificationCenter.postNotificationName(kAddRoomOK, object: nil, userInfo:["message":"Room \(roomName) has been added to \(homeName)"])
      }
    })    
  }
  
  func addAccessory(accessory:HMAccessory, toHome homeName:String!, inRoom roomName:String!) {
    
    NSLog("addAccessory toHome:\(homeName) inRoom:\(roomName)")
    
    var home:HMHome = self.myHomes.filter({h in h.name == homeName}).first as HMHome
    
    NSLog("addAccessory to \(homeName)")

    home.addAccessory(accessory, {
      (e:NSError!) -> Void in
      if let error = e {
        NSLog("Error adding accessory:  \(e.localizedDescription)")
        
        let messageString = "Error adding \(accessory.name):  \(e.localizedDescription)"
        
        self.notificationCenter.postNotificationName(kAddAccessoryError, object: nil, userInfo:["message":messageString,
                                                                                                "accessory":accessory.name])
        
      } else {
        NSLog("Assign accessory to room")
        
        var room:HMRoom = home.rooms.filter({r in r.name == roomName}).first as HMRoom
        
        home.assignAccessory(accessory, toRoom: room, { (error:NSError!) -> Void in
          
          if let e = error {

            self.notificationCenter.postNotificationName(kAddAccessoryError, object: nil)
            
            
          } else {
            NSLog("added")
            
            var messageString = "\(accessory.name) has been added to \(home.name) \(room.name)"
            
            self.notificationCenter.postNotificationName(kAddAccessoryOK, object: nil, userInfo:["message":messageString,
                                                                                                 "accessory":accessory.name])
            
            
          }})
      }
      
    })
  }
  
  func roomsForHomeNamed(homeName:String) -> [String]? {
    
    NSLog("roomsForHomeNamed \(homeName)")
    
    var home:HMHome? = self.myHomes.filter({h in h.name == homeName}).first as HMHome?
    
    var rooms:[String]? = nil
    
    if let h = home {
      rooms = []
      for room in h.rooms {
        NSLog("append \(room.name)")
        rooms!.append(room.name)
      }
      /* Finally, add the house room */
      // Don't do this yet, it causes problems if you try to delete the house room
      // rooms!.append(h.roomForEntireHome().name)
    } else {
      rooms = nil
    }
    
    return rooms

  }
  
  func accessoriesForRoomNamed(roomName:String, inHome homeName:String) -> [String]? {
    ENTRY_LOG()
    
    SLogVerbose("Find accessories for room \(roomName) in home \(homeName)")
    
    var home:HMHome? = self.myHomes.filter({h in h.name == homeName}).first as HMHome?
    
    var accessories:[String]? = nil
    
    if let h = home {
      SLogVerbose("Found home \(homeName)")
      
      var room:HMRoom? = h.rooms.filter({r in r.name == roomName}).first as HMRoom?
      
      if let r = room {
        SLogVerbose("Found room \(roomName)")
        accessories = []
        if r.accessories != nil {
          for accessory in r.accessories {
            SLogVerbose("append \(accessory.name)")
            accessories!.append(accessory.name)
          }
        }
      }
    }
    
    return accessories
    
  }
  
  func homeForHomeNamed(homeName:String) -> HMHome? {
    var home:HMHome? = self.myHomes.filter({h in h.name == homeName}).first as HMHome?
    return home
  }

  
  func homeManager(manager: HMHomeManager!, didAddHome home: HMHome!) {
    NSLog("did add home!")
  }
  
  func homeManagerDidUpdateHomes(manager:  HMHomeManager!) {
    NSLog("homeManagerDidUpdateHomes")

    self.myHomes = self.homeManager.homes
    
    NSLog("Homes = \(self.myHomes)")
    
    self.notificationCenter.postNotificationName(kHomesUpdated, object: nil)
    
  }
  
  func home(home: HMHome!,
    didAddAccessory accessory: HMAccessory!) {
      NSLog("home did add accessory")
  }
  
  
}

let HomeManagerControllerSharedInstanced = HomeManagerController()



